import { Divider } from '@/components/divider'
import React from 'react'
import { Box, Header } from 'zmp-ui'

const HistoryPoint = () => {
    return (
        <>
            <Header title="Lịch sử tính điểm" showBackIcon={true} />

            <Divider />

            <Box>content</Box>
        </>
    )
}

export default HistoryPoint
