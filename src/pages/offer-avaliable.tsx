import { Divider } from '@/components/divider'
import React from 'react'
import { Box, Header } from 'zmp-ui'

const OfferAvaliable = () => {
    return (
        <>
            <Header title="Ưu đãi có thể áp dụng" showBackIcon={true} />

            <Divider />

            <Box>Ưu đãi có thể áp dụng</Box>
        </>
    )
}

export default OfferAvaliable
