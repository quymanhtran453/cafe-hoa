import { Divider } from '@/components/divider'
import OfferTabs from '@/components/offer-tabs'
import { TABS_OFFER } from '@/utils/constants'
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { Box, Header, Icon, Page } from 'zmp-ui'

const AccumulatePoints = () => {
    const [selectedTab, setSelectedTab] = useState<number>(0)
    const navigate = useNavigate()

    return (
        <Page>
            <Header showBackIcon={true} title="Hạng thành viên" />
            <Divider />

            <Box className="bg-background mx-3 rounded-lg">
                <Box className="flex flex-col px-3">
                    <div className="flex items-center justify-between font-medium mt-4">
                        <div>Thành viên</div>
                        <div>0 điểm</div>
                    </div>

                    <Box className="my-9">
                        <div className="flex items-center justify-between gap-2">
                            <div className="h-1 flex-1 bg-black rounded-lg"></div>
                            <div>
                                <Icon icon="zi-star" />
                            </div>
                        </div>

                        <div className="text-xs">Còn 150 điểm nữa để lên Silver</div>
                    </Box>
                </Box>
            </Box>

            <Box className="flex flex-col">
                <Box className="flex flex-col">
                    <div
                        className="flex items-center justify-between px-3 py-4"
                        onClick={() => navigate('/history-points')}
                    >
                        <div className="font-medium">Lịch sử tính điểm</div>
                        <Icon icon="zi-chevron-right" />
                    </div>
                    <div className="h-[0.5px] bg-[#d3d2d2] mx-3"></div>
                </Box>
                <Divider />
                <Box className="flex flex-col">
                    <div
                        className="flex items-center justify-between px-3 py-4"
                        onClick={() => navigate('/offer-avaliable')}
                    >
                        <div className="font-medium">Ưu đãi hiện có</div>
                        <Icon icon="zi-chevron-right" />
                    </div>

                    <Divider />

                    <Box className="px-3 flex justify-around">
                        {TABS_OFFER.map((tab, idx) => (
                            <OfferTabs
                                key={idx}
                                tab={tab}
                                lastIdx={idx === TABS_OFFER.length - 1}
                                selectedTab={selectedTab}
                                onClick={() => {
                                    setSelectedTab(tab.id)
                                }}
                            />
                        ))}
                    </Box>
                    <div className="h-[0.5px] bg-[#d3d2d2] mx-3 mt-2"></div>
                </Box>
            </Box>
        </Page>
    )
}

export default AccumulatePoints
