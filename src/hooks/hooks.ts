import _ from 'lodash';
import { useEffect, useRef, useState } from 'react';
import { matchStatusBarColor } from '@utils/device';
import { EventName, events, Payment } from 'zmp-sdk';
import { useNavigate, useSnackbar } from 'zmp-ui';

export function useMatchStatusTextColor(visible?: boolean) {
    const changedRef = useRef(false);
    useEffect(() => {
        if (changedRef.current) {
            matchStatusBarColor(visible ?? false);
        } else {
            changedRef.current = true;
        }
    }, [visible]);
}

const originalScreenHeight = window.innerHeight;

export function useVirtualKeyboardVisible() {
    const [visible, setVisible] = useState(false);

    useEffect(() => {
        const detectKeyboardOpen = () => {
            setVisible(window.innerHeight + 160 < originalScreenHeight);
        };
        window.addEventListener('resize', detectKeyboardOpen);
        return () => {
            window.removeEventListener('resize', detectKeyboardOpen);
        };
    }, []);

    return visible;
}

export const useHandlePayment = () => {
    const navigate = useNavigate();
    useEffect(() => {
        events.on(EventName.OpenApp, (data) => {
            if (data?.path) {
                Payment.checkTransaction({
                    data: data.path,
                    success: (rs) => {
                        // Kết quả giao dịch khi gọi api thành công
                        const { orderId, resultCode, msg, transTime, createdAt } = rs;
                        console.log({ orderId, resultCode, msg, transTime, createdAt });
                        navigate(data?.path, {
                            state: data,
                        });
                    },
                    fail: (err) => {
                        // Kết quả giao dịch khi gọi api thất bại
                        console.log(err);
                    },
                });
            }
        });

        events.on(EventName.PaymentClose, (data) => {
            const resultCode = data?.resultCode;

            // kiểm tra resultCode trả về từ sự kiện PaymentClose
            // 0: Đang xử lý
            // 1: Thành công
            // -1: Thất bại

            //Nếu trạng thái đang thực hiện, gọi API CheckTransaction.
            if (resultCode === 0) {
                // gọi api checkTransaction để lấy thông tin giao dịch
                Payment.checkTransaction({
                    data: { zmpOrderId: data?.zmpOrderId },
                    success: (rs) => {
                        // Kết quả giao dịch khi gọi api thành công
                        const { orderId, resultCode, msg, transTime, createdAt } = rs;
                    },
                    fail: (err) => {
                        // Kết quả giao dịch khi gọi api thất bại
                        console.log(err);
                    },
                });
            } else {
                // Xử lý kết quả thanh toán thành công hoặc thất bại
                const { orderId, resultCode, msg, transTime, createdAt } = data;
                console.log({ orderId, resultCode, msg, transTime, createdAt });
            }
        });

        events.on(EventName.OnDataCallback, (data) => {
            if (data.appTransID) {
                navigate('/result', {
                    state: data,
                });
            }
        });
    }, []);
};

export function useToBeImplemented() {
    const snackbar = useSnackbar();
    return () =>
        snackbar.openSnackbar({
            type: 'success',
            text: 'Chức năng dành cho các bên tích hợp phát triển...',
        });
}
