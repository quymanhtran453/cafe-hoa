import React from 'react'
import { Icon } from 'zmp-ui'

export type TTabOffer = {
    id: number
    title: string
    points: number
    icon: JSX.Element
    contents?: {
        icon: JSX.Element
        content: string
    }[]
}

export const TABS_OFFER: TTabOffer[] = [
    {
        id: 0,
        title: 'Thành viên',
        points: 0,
        icon: <Icon icon="zi-star" />,
        contents: [
            {
                icon: <Icon icon="zi-check-circle-solid" className="text-[#818080]" />,
                content: 'Giảm 5% cho đơn hàng tiếp theo',
            },
            {
                icon: <Icon icon="zi-check-circle-solid" className="text-[#818080]" />,
                content: 'Miễn phí vận chuyển cho đơn hàng trên 500k',
            },
        ],
    },
    {
        id: 1,
        title: 'Bạc',
        points: 150,
        icon: <Icon icon="zi-star" />,
        contents: [
            {
                icon: <Icon icon="zi-check-circle-solid" className="text-[#818080]" />,
                content: 'Giảm 15% cho đơn hàng tiếp theo',
            },
            {
                icon: <Icon icon="zi-check-circle-solid" className="text-[#818080]" />,
                content: 'Miễn phí vận chuyển cho đơn hàng trên 500k',
            },
        ],
    },
    {
        id: 2,
        title: 'Vàng',
        points: 300,
        icon: <Icon icon="zi-star" />,
        contents: [
            {
                icon: <Icon icon="zi-check-circle-solid" className="text-[#818080]" />,
                content: 'Giảm 100% cho đơn hàng tiếp theo',
            },
            {
                icon: <Icon icon="zi-check-circle-solid" className="text-[#818080]" />,
                content: 'Miễn phí vận chuyển cho đơn hàng trên 500k',
            },
        ],
    },
    {
        id: 3,
        title: 'Bạch kim',
        points: 500,
        icon: <Icon icon="zi-star" />,
    },
]
