import { atom } from 'recoil';
import logo from '@assets/images/logo-real.jpg';
import { Cart } from '@interfaces/cart';
import { Notification } from '@interfaces/notification';
import { Store } from '@interfaces/delivery';

export const selectedCategoryIdState = atom({
    key: 'selectedCategoryId',
    default: 'coffee',
});

export const cartState = atom<Cart>({
    key: 'cart',
    default: [],
});

export const notificationsState = atom<Notification[]>({
    key: 'notifications',
    default: [
        {
            id: 1,
            image: logo,
            title: 'Chào bạn mới',
            content: 'Cảm ơn đã sử dụng ZaUI Coffee, bạn có thể dùng ứng dụng này để tiết kiệm thời gian xây dựng',
        },
        {
            id: 2,
            image: logo,
            title: 'Giảm 50% lần đầu mua hàng',
            content: 'Nhập WELCOME để được giảm 50% giá trị đơn hàng đầu tiên order',
        },
    ],
});

export const keywordState = atom({
    key: 'keyword',
    default: '',
});

export const storesState = atom<Store[]>({
    key: 'stores',
    default: [
        {
            id: 1,
            name: 'VNG Campus Store',
            address: 'Khu chế xuất Tân Thuận, Z06, Số 13, Tân Thuận Đông, Quận 7, Thành phố Hồ Chí Minh, Việt Nam',
            lat: 10.741639,
            long: 106.714632,
        },
        {
            id: 2,
            name: 'The Independence Palace',
            address: '135 Nam Kỳ Khởi Nghĩa, Bến Thành, Quận 1, Thành phố Hồ Chí Minh, Việt Nam',
            lat: 10.779159,
            long: 106.695271,
        },
        {
            id: 3,
            name: 'Saigon Notre-Dame Cathedral Basilica',
            address: '1 Công xã Paris, Bến Nghé, Quận 1, Thành phố Hồ Chí Minh, Việt Nam',
            lat: 10.779738,
            long: 106.699092,
        },
        {
            id: 4,
            name: 'Bình Quới Tourist Village',
            address: '1147 Bình Quới, phường 28, Bình Thạnh, Thành phố Hồ Chí Minh, Việt Nam',
            lat: 10.831098,
            long: 106.733128,
        },
        {
            id: 5,
            name: 'Củ Chi Tunnels',
            address: 'Phú Hiệp, Củ Chi, Thành phố Hồ Chí Minh, Việt Nam',
            lat: 11.051655,
            long: 106.494249,
        },
    ],
});

export const selectedStoreIndexState = atom({
    key: 'selectedStoreIndex',
    default: 0,
});

export const selectedDeliveryTimeState = atom({
    key: 'selectedDeliveryTime',
    default: +new Date(),
});

export const requestLocationTriesState = atom({
    key: 'requestLocationTries',
    default: 0,
});

export const requestPhoneTriesState = atom({
    key: 'requestPhoneTries',
    default: 0,
});
