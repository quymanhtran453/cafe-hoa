import { selector, selectorFamily } from 'recoil';
import { authorize, getLocation, getPhoneNumber, getSetting, getUserInfo } from 'zmp-sdk';
import { Category } from '@interfaces/category';
import { Product, Variant } from '@interfaces/product';
import { calculateDistance } from '@utils/location';
import { calcFinalPrice } from '@utils/product';
import { wait } from '@utils/async';
import categories from '@mocks/categories.json';
import {
    cartState,
    keywordState,
    requestLocationTriesState,
    requestPhoneTriesState,
    selectedStoreIndexState,
    storesState,
} from '@atoms/state';

export const authorizedState = selector({
    key: 'authorized',
    get: async () => {
        const { authSetting } = await getSetting({});
        if (!authSetting['scope.userInfo']) {
            await authorize({ scopes: [] });
        }
    },
});

export const userState = selector({
    key: 'user',
    get: async ({ get }) => {
        get(authorizedState);
        const { userInfo } = await getUserInfo({ avatarType: 'small' });
        return userInfo;
    },
});

export const categoriesState = selector<Category[]>({
    key: 'categories',
    get: () => categories,
});

export const productsState = selector<Product[]>({
    key: 'products',
    get: async () => {
        await wait(2000);
        const products = (await import('@mocks/products.json')).default;
        const variants = (await import('@mocks/variants.json')).default as Variant[];
        return products.map(
            (product) =>
                ({
                    ...product,
                    variants: variants.filter((variant) => product.variantId.includes(variant.id)),
                } as Product)
        );
    },
});

export const recommendProductsState = selector<Product[]>({
    key: 'recommendProducts',
    get: ({ get }) => {
        const products = get(productsState);
        return products.filter((p) => p.sale);
    },
});

export const productsByCategoryState = selectorFamily<Product[], string>({
    key: 'productsByCategory',
    get:
        (categoryId) =>
        ({ get }) => {
            const allProducts = get(productsState);
            return allProducts.filter((product) => product.categoryId.includes(categoryId));
        },
});

export const totalQuantityState = selector({
    key: 'totalQuantity',
    get: ({ get }) => {
        const cart = get(cartState);
        return cart.reduce((total, item) => total + item.quantity, 0);
    },
});

export const totalPriceState = selector({
    key: 'totalPrice',
    get: ({ get }) => {
        const cart = get(cartState);
        return cart.reduce((total, item) => total + item.quantity * calcFinalPrice(item.product, item.options), 0);
    },
});

export const resultState = selector<Product[]>({
    key: 'result',
    get: async ({ get }) => {
        const keyword = get(keywordState);
        if (!keyword.trim()) {
            return [];
        }
        const products = get(productsState);
        await wait(500);
        return products.filter((product) => product.name.trim().toLowerCase().includes(keyword.trim().toLowerCase()));
    },
});

export const nearbyStoresState = selector({
    key: 'nearbyStores',
    get: ({ get }) => {
        // Get the current location from the locationState atom
        const location = get(locationState);

        // Get the list of stores from the storesState atom
        const stores = get(storesState);

        // Calculate the distance of each store from the current location
        if (location) {
            const storesWithDistance = stores.map((store) => ({
                ...store,
                distance: calculateDistance(location.latitude, location.longitude, store.lat, store.long),
            }));

            // Sort the stores by distance from the current location
            const nearbyStores = storesWithDistance.sort((a, b) => a.distance - b.distance);

            return nearbyStores;
        }
        return [];
    },
});

export const selectedStoreState = selector({
    key: 'selectedStore',
    get: ({ get }) => {
        const index = get(selectedStoreIndexState);
        const stores = get(nearbyStoresState);
        return stores[index];
    },
});

export const locationState = selector<{ latitude: string; longitude: string } | false>({
    key: 'location',
    get: async ({ get }) => {
        const requested = get(requestLocationTriesState);
        if (requested) {
            const { latitude, longitude, token } = await getLocation({
                fail: console.warn,
            });
            if (latitude && longitude) {
                return { latitude, longitude };
            }
            if (token) {
                console.warn('Sử dụng token này để truy xuất vị trí chính xác của người dùng', token);
                console.warn(
                    'Chi tiết tham khảo: ',
                    'https://mini.zalo.me/blog/thong-bao-thay-doi-luong-truy-xuat-thong-tin-nguoi-dung-tren-zalo-mini-app'
                );
                console.warn('Giả lập vị trí mặc định: VNG Campus');
                return {
                    latitude: '10.7287',
                    longitude: '106.7317',
                };
            }
        }
        return false;
    },
});

export const phoneState = selector<string | boolean>({
    key: 'phone',
    get: async ({ get }) => {
        const requested = get(requestPhoneTriesState);
        if (requested) {
            const { number, token } = await getPhoneNumber({ fail: console.warn });
            if (number) {
                return number;
            }
            console.warn('Sử dụng token này để truy xuất số điện thoại của người dùng', token);
            console.warn(
                'Chi tiết tham khảo: ',
                'https://mini.zalo.me/blog/thong-bao-thay-doi-luong-truy-xuat-thong-tin-nguoi-dung-tren-zalo-mini-app'
            );
            console.warn('Giả lập số điện thoại mặc định: 0337076898');
            return '0337076898';
        }
        return false;
    },
});
