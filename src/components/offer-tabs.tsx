import { TTabOffer } from '@/utils/constants'
import React from 'react'
import { Box } from 'zmp-ui'

const OfferTabs = ({
    tab,
    lastIdx,
    selectedTab,
    onClick,
}: {
    tab: TTabOffer
    lastIdx: boolean
    selectedTab?: number
    onClick?: () => void
}) => {
    return (
        <Box className="flex flex-col items-center relative w-full" onClick={onClick}>
            {tab.icon}
            <div className={`${selectedTab === tab.id ? 'text-primary' : 'text-[#818080]'} font-medium text-sm mt-2`}>
                {tab.title}
            </div>
            {!lastIdx && <span className="w-[0.5px] h-[90%] bg-[#d3d2d2] absolute right-0"></span>}

            {selectedTab === tab.id && <span className="h-1 w-[90%] bg-[#d3d2d2] absolute bottom-[-8px]"></span>}

            {tab.contents && selectedTab === tab.id && (
                <Box className="flex flex-col gap-2 fixed top-[65%] left-[3%] w-[400%] items-start">
                    {tab.contents.map((content, idx) => (
                        <Box key={idx} className="flex gap-2">
                            {content.icon}
                            <div>{content.content}</div>
                        </Box>
                    ))}
                </Box>
            )}
        </Box>
    )
}

export default OfferTabs
