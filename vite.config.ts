import { defineConfig } from 'vite';
import tsconfigPaths from 'vite-tsconfig-paths';
import react from '@vitejs/plugin-react';
import path from 'path';
// https://vitejs.dev/config/
export default () => {
    return defineConfig({
        root: './src',
        base: '',
        plugins: [tsconfigPaths(), react()],
        resolve: {
            alias: {
                '@': path.resolve(__dirname, './src'),
                '@pages': path.resolve(__dirname, './src/pages'),
                '@assets': path.resolve(__dirname, './assets'),
                '@interfaces': path.resolve(__dirname, './src/interfaces'),
                '@enums': path.resolve(__dirname, './src/enums'),
                '@slices': path.resolve(__dirname, './src/store/slices'),
                '@components': path.resolve(__dirname, './src/components'),
                '@utils': path.resolve(__dirname, './src/utils'),
                '@hooks': path.resolve(__dirname, './src/hooks'),
                '@atoms': path.resolve(__dirname, './src/atoms'),
                '@selectors': path.resolve(__dirname, './src/selectors'),
                '@mocks': path.resolve(__dirname, './mock'),
            },
        },
    });
};
